import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    root: ({ start,end }) => ({
        fontSize: 60,
       margin: 20,
        color: 'blue',
        ...(start && {
            color: 'Green',
        }),
        ...(end && {
            color: 'Red',
        }),
    }),
    block:{
        color: 'black !important'
    } ,
    yellow:{
        color: '#ffbf00 !important'
    }
}));


function EachList({i,j,selectBlock,blockList,result,length}) {

    i = i.toString();
    j = j.toString();
    let start = i + j === '00';
    let end = i + j === (length - 1).toString() + (length - 1).toString();

    const [block,setBlock] = useState(false);
    const [isPath,setIsPath] = useState(false);

    const classes = useStyles({ start,end});

    function isBlock(){
        i = i.toString();
        j = j.toString();
        if(blockList && blockList.length){
            const result = blockList.filter(each => each === i+j);
            if(result && result.length) setBlock(true);
            else return   setBlock(false);
        }
        else return  setBlock(false);
    }
    function checkIsPath(){
        i = i.toString();
        j = j.toString();
        if(result && result.length){
            const resultData = result.filter(each => each === i+j);
            // console.log('result',resultData)
            if(resultData && resultData.length) setIsPath(true);
            else return   setIsPath(false);
        }
        else return  setIsPath(false);
    }



    useEffect(() =>{
        isBlock();
    },[blockList]);

    useEffect(() =>{
        checkIsPath();
    },[result]);


    return(
        <span
            className={classes.root + ' ' + (block ? classes.block  : '') + ' ' + (isPath ?  classes.yellow  : '')}
            onClick={selectBlock}>
            ☻
        </span>
    );
}
export default EachList;