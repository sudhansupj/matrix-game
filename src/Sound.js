import React from 'react';

class Sound extends React.Component {
    constructor(props) {
        super(props);
        window.moveAudioRef = React.createRef();
        window.finishAudioRef = React.createRef();
        window.errorAudioRef = React.createRef();
    }
    render() {
        return (
            <div style={{height: 0, visibility: 'hidden'}}>
                <audio
                    ref={window.moveAudioRef}
                    src={'/pop.mp3'}
                />
                <audio
                    ref={window.errorAudioRef}
                    src={'/Oddbounce.ogg'}
                />
                <audio
                    ref={window.finishAudioRef}
                    src={'/gotitem.mp3'}
                />
            </div>
        );
    }
}
export default Sound;

