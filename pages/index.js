import React,{useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import EachList from '../src/EachList';
import {Box} from "@material-ui/core";
import Sound from "../src/Sound";
import dynamic from 'next/dynamic'

function playSound(audioRef){
  if(audioRef && audioRef.current && audioRef.current.play) {
    if(audioRef.current.stop) audioRef.current.stop();
    audioRef.current.play();
  }
}

const getResultArray  = (length, blockList = []) => {

  let i = 0;
  let j = 0;
  let resultArray = [];
  const resultBlockList = [...blockList];

  function isBlocked(i,j) {
    i = i.toString();
    j = j.toString();
    const result = resultBlockList.filter(each => each === i+j);
    return !!result.length;
  }

  function notInWay(i, j) {
    const result = resultArray.find(([eachI, eachJ]) => eachI === i && eachJ === j);
    return !result;
  }

  function findWay(i,j, previous = null) {
    const result = [];
    if (j < length - 1) {
      const nextJ = j + 1;
      if (!isBlocked(i, nextJ) )  result.push([i, nextJ]);
    }
    if (i < length - 1) {
      const nextI = i + 1;
      if (!isBlocked(nextI, j)) result.push([nextI, j]);
    }
    if (j > 0) {
      const prevJ = j - 1;
      if (previous) {
        // const [previousI, previousJ] = previous;
        if (notInWay(i, prevJ))
          if (!isBlocked(i, prevJ)) result.push([i, prevJ]);
      } else {
        if (!isBlocked(i, prevJ)) result.push([i, prevJ]);
      }
    }
    if (i > 0) {
      const prevI = i - 1;
      if (previous) {
        // const [previousI, previousJ] = previous;
        if (notInWay(prevI, j))
          if (!isBlocked(prevI, j)) result.push([prevI, j]);
      } else {
        if (!isBlocked(prevI, j)) result.push([prevI, j]);
      }
    }
    return result;
  }

  function startMatrix() {
    if (resultArray.length > 60) return null;
    const wayResult = findWay(i, j, resultArray.length && resultArray[resultArray.length - 2] ? resultArray[resultArray.length - 2] : null);
    if (wayResult.length) {
      const [newI, newJ] = wayResult[0];
      resultArray.push([newI, newJ]);
      if ((newI === length - 1) && (newJ === length - 1)) return resultArray;
      i = newI;
      j = newJ;
      return startMatrix();
    } else {
      if (resultArray && resultArray.length) {
        const [blockedI, blockedJ] =  resultArray[resultArray.length - 1];
        resultBlockList.push(`${blockedI}${blockedJ}`);
        if(resultArray.length === 1) resultArray = [];
        else resultArray.splice(resultArray.length - 1, 1);
        const [newI = 0, newJ = 0] = resultArray.length ? resultArray[resultArray.length - 1] : resultArray;
        i = newI;
        j = newJ;
        return startMatrix();
      } else {
        return [];
      }
    }
  }

  return startMatrix();
};


function Index() {

  const [blockList,setBlockList] = useState([]);
  const [length,setLength] = useState(4);
  const [result,setResult] = useState([]);
  const [matrix,setMatrix] = useState([0,1,2,3]);


  function isBlocked(i,j) {
    i = i.toString();
    j = j.toString();
    const result = blockList.filter(each => each === i+j);
    return !!result.length;
  }

  const selectBlock = (i,j) =>{
    i = i.toString();
    j = j.toString();
    if(!isBlocked(i,j)) blockList.push(i+j);
    else blockList.map((each,index) => each === i+j && blockList.splice(index,1));
    setBlockList([...blockList]);
  };

  const handleGetResult = () =>{
    let _allData;
    const data = getResultArray(matrix.length, blockList);
    _allData =  data.map(each => `${each[0]}${each[1]}`);
    if(!_allData || !_allData.length ) {
      playSound(window.errorAudioRef);
      alert('Path not found.')
      return
    }
    let i = 0;
    const animate =  () => {
      if (i < _allData.length) {
        setResult(_allData.slice(0, i));
        playSound(window.moveAudioRef);
        i++;
        setTimeout(animate, 600);
      }
      else {
        playSound(window.finishAudioRef);
      }
    };
    animate();
  };

  const handleReset = () =>{
    setBlockList([]);
    setResult([]);
  }

  function handleCreateArray() {
    if(length < 2 || length > 20) return alert('Number should be 2 to 20')
    handleReset();
      let i = 0;
      let array = [];
      for (i = 0; i < length ; i++){
        array.push(i)
      };
      setMatrix([...array]);
  }

  return(
      <div style={{margin:40}}>
        <Box display={'flex'} >
          <Box width="30%">
            <TextField value={length} onChange={(e) => setLength(e.target.value)} type={'number'} fullWidth placeholder={'Add number of matrix you want to play'} id="outlined-basic"  variant="outlined" />
          </Box>
          <Button onClick={handleCreateArray} style={{marginLeft:10}} variant="contained" color="primary">
            add matrix
          </Button>
        </Box>
        {
          matrix.map((i,iIndex) =>
              <div key={i} style={{marginTop:10}}>
                {
                  matrix.map((j,JIndex) =>
                      <EachList length={matrix.length} result={result} blockList={blockList} i={iIndex} j={JIndex} key={j} selectBlock={() => selectBlock(iIndex,JIndex)}/>
                  )
                }
              </div>
          )
        }
        <Button onClick={handleGetResult} style={{marginLeft:10}} variant="contained" color="primary">
          Start
        </Button><Button onClick={handleReset} style={{marginLeft:10}} variant="contained" color="primary">
        Reset
        </Button>
        <Sound/>
      </div>
  );
}


export default dynamic(() => Promise.resolve(Index), {
  ssr: false
})